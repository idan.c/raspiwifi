import argparse


def main(ip: str, mask: str, ssid: str, psk: str):
    print(ip, mask, ssid, psk)
    return


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Configures an access point on a raspberry pi device')
    parser.add_argument('--ip',
                        default="10.0.0.0",
                        dest='ip',
                        help='The desired IP of the wireless network (default: 10.0.0.0)')

    parser.add_argument('--mask',
                        default="255.255.255.0",
                        dest='mask',
                        help='The desired mask of the wireless network (default: 255.255.255.0)')

    parser.add_argument('--ssid',
                        default="PiFi",
                        dest='ssid',
                        help='The desired ssid of the wireless network (default: PiFi)')

    parser.add_argument('-P', '--passphrase',
                        default="",
                        dest='psk',
                        help='The desired psk passphrase of the wireless network (default: empty)')
    args = parser.parse_args()

    main(args.ip, args.mask, args.ssid, args.psk)
